SYSTEM=$(shell uname)
NBCORES=$(shell nproc)
MAKE:=@$(MAKE) --silent --no-print-directory
CC=colorgcc


# Variables

INSTALL_PATH=$(HOME)/bin_leto_prom/

# -> SHOULDN'T APPEAR ANYMORE (there is no GUI at all)!!!
GTK_INCLUDES=$(shell pkg-config --cflags gtk+-2.0 gthread-2.0)
GTK_LIBS=$(shell pkg-config --libs gtk+-2.0 gthread-2.0)
### !!!!!

PROMETHE_INCLUDES=$(shell cat $(INSTALL_PATH)/include/promethe_includes/*)
INCLUDES=$(PROMETHE_INCLUDES) $(GTK_INCLUDES) -I$(INSTALL_PATH)/include -I./libpromc/src
DEFINES= -DAVEUGLE -D_REENTRANT -D_GNU_SOURCE -DDAEMON_COM -D$(SYSTEM) -DSYMBOLIQUE_VERSION
LIBRARIES=-L$(INSTALL_PATH)/lib -lblc  -lpromc -lrt -lprom-script-symb -lprom-graphics -lmxml $(GTK_LIBS) -ldl -lm -Wl,-rpath,$(INSTALL_PATH)/lib

CFLAGS:=$(DEFINES) $(INCLUDES)
CFLAGS_DEBUG:=-g $(CFLAGS)
CFLAGS:=-O2 $(CFLAGS)

LD_FLAGS=$(LIBRARIES) -rdynamic

SOURCES=$(shell find src/ -depth -type f -name '*.c')
OBJECTS=$(SOURCES:src/%.c=bin/%.o)


# Public Targets

default:
	$(MAKE) --jobs=$(NBCORES) all

all: libpromc dist/promc dist/promc-dbg

clean:
	@rm -rf bin
	@$(MAKE) -C libpromc clean

install: all
	mkdir -p $(INSTALL_PATH)/
	ln -sf $(PWD)/dist/promc $(INSTALL_PATH)/promc
	ln -sf $(PWD)/dist/promc-dbg $(INSTALL_PATH)/promc-dbg


dist/promc: $(OBJECTS:bin/%=bin/Release/%)
	@mkdir -p dist
	@echo "Link $@"
	@$(CC) -o $@ $^ $(LD_FLAGS)

dist/promc-dbg: $(OBJECTS:bin/%=bin/Debug/%)
	@mkdir -p dist
	@echo "Link $@"
	@$(CC) -o $@ $^ $(LD_FLAGS)

.PHONY: libpromc
libpromc:
	@$(MAKE) --silent -C $@ 



# Generic rules

bin/Release/%.o: src/%.c
	@mkdir -p `dirname $@`
	@echo "Compile $< -> $@"
	@$(CC) -MMD -MP -MF $(@:%.o=%.d) -o $@ -c $< $(CFLAGS)
-include $(SRC:src/%.c=bin/Release/%.d)

bin/Debug/%.o: src/%.c
	@mkdir -p `dirname $@`
	@echo "Compile $< -> $@"
	@$(CC) -MMD -MP -MF $(@:%.o=%.d) -o $@ -c $< $(CFLAGS_DEBUG)
-include $(SRC:src/%.c=bin/Debug/%.d)
