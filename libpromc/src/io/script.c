/*
 * script.c
 *
 *  Created on: 21 avr. 2016
 *      Author: jfellus
 */

#include "script.h"



int read_one_group(FILE * f, type_groupe * groupe, char *macro_name, struct hsearch_data *hashtab, char** retour_nom_xyz) {
  char ligne[TAILLE_CHAINE];
  char nbre[TAILLE_CHAINE];
  char seuil[TAILLE_CHAINE];
  char no_name[SIZE_NO_NAME];
  type_noeud_comment *first_comment = NULL;
  int i;
  int ret;
  void *exist = NULL;

  first_comment = (groupe->comment); /* le champs comment est initialise a NULL */
  first_comment = read_line_with_comment(f, first_comment, ligne);


  ret = sscanf(ligne, "groupe = %s , type = %d , nbre neurones = %s , seuil = %s\n", no_name, &groupe->type, nbre, seuil);
  if (ret != 4) EXIT_ON_ERROR("Reading first line of group in the script file : '%s'", ligne);

  Str2MY_Int(groupe->nbre, nbre);
  Str2MY_Float(groupe->seuil, seuil);
  if (macro_name) sprintf(groupe->no_name, "%s_%s", macro_name, no_name);
  else strcpy(groupe->no_name, no_name);

  dprints("groupe = %s , type = %d , nbre neurones = %u , seuil = %f\n", groupe->no_name, groupe->type, groupe->nbre, groupe->seuil);


  if (lookup_hash_table(hashtab, groupe->no_name))
    EXIT_ON_ERROR(" Erreur le label %s du groupe %d a deja ete utilise par le groupe entre en no %s \n", groupe->no_name, nbre_groupes_lus, groupe->no_name);

  insert_hash_table(hashtab, groupe->no_name, groupe);

  read_one_group_second_part(f, groupe, first_comment, retour_nom_xyz);

#ifndef SYMBOLIQUE_VERSION
  return groupe->nbre;
#else
  return -1;
#endif
}

