/*
 * script.h
 *
 *  Created on: 21 avr. 2016
 *      Author: jfellus
 */

#ifndef LIBPROMC_SRC_IO_SCRIPT_H_
#define LIBPROMC_SRC_IO_SCRIPT_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* pour mac OS */
#ifdef Darwin
#define __USE_GNU
#include "search_gnu.h"
#else
#include <search.h>  /* classique linux */
#endif

#include <outils_script.h>

extern int nbre_groupes_lus;

int read_one_group(FILE * f1, type_groupe * groupe, char *macro_name, struct hsearch_data *hashtab, char** retour_nom_xyz);



#endif /* LIBPROMC_SRC_IO_SCRIPT_H_ */
