/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include "gere_coudes.h"
#include "string.h"
#include "basic_tools.h"
#include "libpromc.h"
#include "model/group.h"
#include "model/script.h"


/* Fonction qui retire l'extension .script */
void get_base_path_name(char *filename)
{
	char *pt;

	/* recherche l'adr du debut de l'extension */
	pt = rindex(filename, '.');
	if (pt != NULL) *pt = '\0'; /* on oublie l'extension du fichier: on met fin de chaine au debut de l'extension*/
}



/* Fonction qui retire l'extension .script et le path d'un nom de fichier */
void get_base_name(char *filename)
{
	char *pt;
	char basename[NAME_MAX+1];

	/* recherche l'adr du debut de l'extension */
	pt = rindex(filename, '.');
	if (pt != NULL) *pt = '\0'; /* on oublie l'extension du fichier: on met fin de chaine au debut de l'extension*/
	pt = rindex(filename, '/'); /* trouve le dernier '/' pour enlever le path du nom*/
	if (pt != NULL){
		strcpy(basename, &(pt[1])); /* on saute le '/' */
		strcpy(filename, basename);
	}

}


/* renvoie le no du prochain plan libre pour y mettre une macro */
/* si no_macro_used est different de NULL alors on recupere aussi un no de macro propose par find_new_plane_for_new_macro */
int find_new_plane_for_new_macro(int *no_macro_used)
{
	int no_macro_used_2;
	int selected_plane;

	if (no_macro_used == NULL) no_macro_used = &no_macro_used_2;

	*no_macro_used = sc->nbre_macro_lues;
	selected_plane = (100 + sc->nbre_macro_lues * 7);

	while (plane_used(selected_plane) == 1)
	{
		printf("Attention plan %d deja utilise j'en cherche un autre \n", selected_plane);
		(*no_macro_used)++;
		selected_plane = (100 + (*no_macro_used) * 7);
	}

	return selected_plane;
}


/* verifie si un plan d'affichage est deja utilise par au moins un groupe */
int plane_used(int plan)
{
	type_groupe *groupe;

	groupe = sc->deb_groupe;
	while (groupe != NULL)
	{
		if (abs(groupe->reverse) == plan) return 1;
		groupe = groupe->s;
	}
	return 0;
}


/* ici on initialise tous les coudes pour les liaisons n'ayant pas de liste de coudes definies */
void initialise_liaisons()
{
#ifndef AVEUGLE
	type_liaison *liaison_courante;
	liaison_courante = sc->deb_liaison;

	while (liaison_courante != NULL)
	{
		initialise_coudes_liaison(liaison_courante);
		liaison_courante = liaison_courante->s;
	}
#endif
}



/* la fonction renvoie 1 si les groupes en entree et en sortie du lien sont dans la zone de selection */
/* la fonction renvoie 0 sinon */

int selected_link(type_liaison *liaison, TxDonneesFenetre *onglet_leto)
{
#ifndef AVEUGLE
	type_groupe *groupe_depart, *groupe_arrivee;

	if (liaison->depart == -1 && liaison->arrivee == -1) return 0; /* avant =1;  pour eviter de tjs selectionner le lien -1 a -1 */

	groupe_depart = trouver_groupe_par_nom(liaison->depart_name, onglet_leto);
	if (groupe_depart == NULL)
	{
		EXIT_ON_ERROR("input group %s does not exist \n", liaison->depart_name);
	}
	if (is_selected(groupe_depart) == NULL) return 0;

	groupe_arrivee = trouver_groupe_par_nom(liaison->arrivee_name, onglet_leto);
	if (groupe_arrivee == NULL)
	{
	   EXIT_ON_ERROR(" output group %s does not exist \n", liaison->arrivee_name);
	}

	if (is_selected(groupe_arrivee) == NULL) return 0;
	return 1;
#else
	(void)liaison;
	(void)onglet_leto;
	return 0;
#endif
}



/*------------------------------------------------------------------*/
/* calcule les coordonnees ou devront etre afficher les differents  */
/* groupes dans promethe. Si debug<0 => doit etre calculer sinon    */
/* prendre les valeurs deja utilisees                               */
/*------------------------------------------------------------------*/

void prepare_affichage()
{
	int taille_bloc = 128;
	type_groupe *groupe;
	int orig_x, orig_y;

	orig_x = 0;
	orig_y = 0;
	groupe = sc->deb_groupe;
	while (groupe != NULL)
	{
		if (groupe->debug > 0)
		{
			groupe->p_posx = orig_x * (taille_bloc + 20);
			groupe->p_posy = orig_y * (taille_bloc + 20) + 20;
		}
		orig_x++;
		if (orig_x > 5)
		{
			orig_x = 0;
			orig_y++;
		}
		groupe = groupe->s;
	}
}








/*-----------------------------------------------------------------*/
/*                CREATION VECTEUR                                 */
/*-----------------------------------------------------------------*/

type_vecteur_entier creer_vecteur(int n)
{
	int i, a;
	type_vecteur_entier t2;
	a = n + 2;
	t2 = (int *) calloc(a, sizeof(int));
	if (t2 == NULL)
	{
		printf("\n\n ERREUR : le Calloc a echoue !?!?!?!?!?!?!?....\n\ncreer_vecteur\n");
		exit(0);
	}
	for (i = 0; i < a; i++)
		t2[i] = 0;
	return (t2);
}

void free_vecteur(type_vecteur_entier *t)
{
	free(t);
}
