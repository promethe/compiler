/*
 * group.h
 *
 *  Created on: 21 avr. 2016
 *      Author: jfellus
 */

#ifndef LIBPROMC_SRC_MODEL_GROUP_H_
#define LIBPROMC_SRC_MODEL_GROUP_H_

#include <reseau.h>
#include "script.h"

type_groupe *group_create(type_groupe * prec);


#endif /* LIBPROMC_SRC_MODEL_GROUP_H_ */
