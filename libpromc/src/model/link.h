/*
 * link.h
 *
 *  Created on: 21 avr. 2016
 *      Author: jfellus
 */

#ifndef LIBPROMC_SRC_MODEL_LINK_H_
#define LIBPROMC_SRC_MODEL_LINK_H_


#include <reseau.h>
#include "script.h"

type_liaison *link_create(type_liaison * prec);
void link_free(type_liaison * liaison);


#endif /* LIBPROMC_SRC_MODEL_LINK_H_ */
