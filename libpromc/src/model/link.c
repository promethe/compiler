/*
 * link.c
 *
 *  Created on: 21 avr. 2016
 *      Author: jfellus
 */



#include "link.h"


type_liaison *link_create(type_liaison * prec) {
	type_liaison *nouv;
    nouv = ALLOCATION(type_liaison);
	if (prec != NULL) prec->s = nouv;
	memset(nouv, 0, sizeof(type_liaison));
	return (nouv);
}

void link_free(type_liaison * liaison) {
	if (liaison->s != NULL) link_free(liaison->s);
	free(liaison);
}
