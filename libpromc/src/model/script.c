/*
 * script.c
 *
 *  Created on: 21 avr. 2016
 *      Author: jfellus
 */




#include "script.h"
#include <libpromc.h>
#include <outils_script.h>





type_groupe *trouver_groupe(int gpe) {
	if (sc->deb_groupe == NULL) EXIT_ON_ERROR("trouver_groupe(): Script has no groups ! \n");
	type_groupe *groupe = sc->deb_groupe;
	while (groupe != NULL) {
		/*printf("groupe %d -> %s\gpe",groupe->no,groupe->no_name);*/
		if (groupe->no == gpe) return (groupe);
		groupe = groupe->s;
	}

	EXIT_ON_ERROR("Error (trouver_groupe): group %d not found \n", gpe);
	return NULL; /* pour eviter erreur systeme */
}

type_groupe *trouver_groupe_par_nom(char *nom, TxDonneesFenetre *onglet_leto) {
	return find_group_associated_to_symbolic_name(nom, onglet_leto->hashtab);
}



void affiche_liste_groupes()
{
	type_groupe *groupe;
	groupe = sc->deb_groupe;
	while (groupe != NULL)
	{
		groupe = groupe->s;
	}
}
