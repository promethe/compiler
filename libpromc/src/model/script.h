/*
 * script.h
 *
 *  Created on: 21 avr. 2016
 *      Author: jfellus
 */

#ifndef LIBPROMC_SRC_MODEL_SCRIPT_H_
#define LIBPROMC_SRC_MODEL_SCRIPT_H_

#include <reseau.h>
#include "group.h"
#include "link.h"
#include "../gennet.h"



type_groupe *trouver_groupe(int gpe);
type_groupe *trouver_groupe_par_nom(char *nom, TxDonneesFenetre *onglet_leto);

void affiche_liste_groupes();


#endif /* LIBPROMC_SRC_MODEL_SCRIPT_H_ */
