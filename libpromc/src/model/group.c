/*
 * group.c
 *
 *  Created on: 21 avr. 2016
 *      Author: jfellus
 */


#include "group.h"
#include "../libpromc.h"


type_groupe *group_create(type_groupe * prec) {
	type_groupe *nouv = (type_groupe *) ALLOCATION(type_groupe);
	memset(nouv, 0, sizeof(type_groupe));

	if (prec != (type_groupe *) NULL) prec->s = nouv;
	nouv->s = (type_groupe *) NULL;
	nouv->no = ++sc->last_groupe_number; /*sc->nbre_groupe;*/

	MY_FloatAffect(nouv->seuil, 0.);
	MY_FloatAffect(nouv->alpha, 0.);
	MY_FloatAffect(nouv->learning_rate, 1.);
	MY_FloatAffect(nouv->simulation_speed, 1.);
	nouv->type = No_Fonction_Algo;
	MY_IntAffect(nouv->type2, 0);
	nouv->ech_temps = 0;

	MY_IntAffect(nouv->taillex, 1);
	MY_IntAffect(nouv->tailley, 1);
	MY_IntAffect(nouv->nbre, 1);

	MY_FloatAffect(nouv->tolerance, 0.);
	MY_FloatAffect(nouv->seuil, 0.);

	MY_IntAffect(nouv->dvp, -1);
	MY_IntAffect(nouv->dvn, -1);

	MY_FloatAffect(nouv->nbre_de_1, -1.);
	MY_FloatAffect(nouv->sigma_f, -1.);
	MY_FloatAffect(nouv->noise_level, 0.);

	nouv->reverse = 0;
	strcpy(nouv->nom, "???");

	nouv->debug = -1; /* la position du groupe pour promethe doit etre calculee */
	return nouv;
}
