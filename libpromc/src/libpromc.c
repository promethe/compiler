/*
 * libpromc.c
 *
 *  Created on: 21 avr. 2016
 *      Author: jfellus
 */

#include "libpromc.h"
#include <mode_link_data_def.h>





//////////////////


void script_init_data(donnees_script *le_script) {
  le_script->premiere_lecture = 0;

  /** Init tab of mode link depending on group type */
  init_group_mode_link_tab();

  le_script->flag_symb = 0;

  /* script.c */
  le_script->first_comment_group = NULL;
  le_script->first_comment_link = NULL;
  le_script->nbre_macro_lues=0;
  le_script->nbre_groupes_lus=0;
  le_script->last_groupe_number=0;

  /* init de la partie reseau de neurone */
  le_script->nbre_liaison = 0;
  le_script->nbre_neurone = 0;
  le_script->nbre_groupe = 0;

  le_script->deb_groupe = le_script->fin_groupe = NULL;
  le_script->deb_liaison = le_script->fin_liaison = NULL;

/*---------------------------------------------------------------*/

  le_script->nbre_couche = 1;            /* pour compatibilite avec vieux reseau */
  le_script->t = creer_vecteur(1);
  le_script->ngc = creer_vecteur(1);
  le_script->n = creer_vecteur(1);
  le_script->backup_id = 0;
}
