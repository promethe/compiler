/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef SCRIPT_H
#define SCRIPT_H

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <script_parserxml.h>
#include <linux/limits.h>
#include <stdarg.h>

#include "model/model.h"



void script_load(donnees_script *script, char *filename, int recursive_load, struct hsearch_data *hash_table);

 int read_macro(char *base_base, char *nom_fichier,int px, int py, int relatif, int mode, int *selected_plane, struct hsearch_data *hash_table);
 void load_script(donnees_script *script, char *filename, int recursive_load, struct hsearch_data *hash_table);
 int script_set_modified(donnees_script *script);

 void script_read_draw();


/* variables globales pour la gestion des commentaires non attaches a un groupe */
/* Il s'agit des commentaires precedent le debut des groupes et des commentaires */
/* precedent le debut des liens (si c'est possible! / correspond fin d'un groupe non?) */

#ifndef AVEUGLE // Added by @jfellus
extern void save_script(int comment,int save_sub_networks, TxDonneesFenetre *onglet_leto);
extern void save_script_selected(int comment,int save_sub_networks,char *nom, TxDonneesFenetre *onglet_leto);
extern void lecture(int recursive_load,TxDonneesFenetre *onglet_leto);
#endif

extern void sauvegarde();
extern void sauvegarde_avec_comment();










/* outils */

void fatal_error(const char *name_of_file, const char* name_of_function,  int numero_of_line, const char *message, ...);


extern type_tableau creer_reseau(int );
extern void free_reseau(donnees_script *script, type_tableau t2);

extern type_noeud **creer_groupe(int );
extern void free_groupe(type_noeud** t2);

extern type_coeff *creer_coeff();
extern void free_coeff(type_coeff *coeff);

extern type_noeud *creer_noeud();
extern void free_noeud(type_noeud *t);

extern type_matrice creer_matrice(int n, int m);
extern void free_matrice(type_matrice *t, int n);

extern type_vecteur_entier creer_vecteur(int n);
extern void free_vecteur(type_vecteur_entier *t);

extern void free_liste_groupes(type_groupe *groupe);


extern type_groupe *trouver_groupe_ante_par_nom(char *nom);

extern type_liaison *trouver_liaison_par_nom(char *no_groupe_depart_name,char *no_groupe_arrivee_name);


extern type_coeff *pointe_vers_dernier(int j);
extern int compte_neurone();
extern float alea(float x);
extern int sorte_liaison(int No_liaison);

extern void prepare_affichage();
extern void replace_tout();

#ifndef AVEUGLE // @jfellus : WTF ??!! added...
extern int selected_link(type_liaison *liaison, TxDonneesFenetre *onglet_leto);
#endif

extern int plane_used(int plan);
extern int find_new_plane_for_new_macro(int *no_macro_used);


void initialise_liaisons();

/* fonction a moi */
extern void get_base_path_name(char *filename);
extern void get_base_name(char *filename);
void creer_liaison_1_vers_patern_neurone(donnees_script *sc,type_groupe * groupe, type_liaison * liaison, int no_premier_neurone, int pas, int pas_entree, int no_voie, int no_gpe_liaison, int s_deb, int s_fin, int e_deb, int e_fin, int *pos1);
#endif
