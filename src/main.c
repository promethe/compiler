/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>

// #include "graphic_Tx.h" <-- ??? WE DON'T HAVE ANY GUI, COME ON !!
#include "reseau.h"
#include "../libpromc/src/libpromc.h"
#include "utils.h"



int flag_save_res = 1; /*flag de compilation  : 1 par defaut compile le res et l'enregistre.  0 compile le res mais ne l'enregistre pas*/




int main(int argc, char *argv[]) {
	if (argc != 3) {
		if(argc == 4 && strcmp(argv[3],"--no_res")==0 ) {
			flag_save_res = 0;
			dprints("cc_leto  %s %s %u \n",argv[1],argv[2],flag_save_res);
		}
		else {
			EXIT_ON_ERROR("\nincorrect number of arguments ! \n cc_leto system.script systen.res [--no_res]\n");
		}
	}
	if(argc == 3) printf ("cc_leto : %s %s \n",argv[1],argv[2]);

	dprints("initial value of the pseudo random generator : ");
	int val;
	if (scanf("%d", &val) != 1) PRINT_WARNING("Wrong value (not integer) of generator");
	srand48((long) val);

	sc->draw[0]='\0';

	script_init_data(sc);

	memcpy(sc->nomfich1, argv[1], (strlen(argv[1])+1) * sizeof(char));
	memcpy(sc->freseau, argv[2], (strlen(argv[2])+1) * sizeof(char));
	strcpy(sc->directory, "."); /* Il faudrait prendre le repertoire de monfich AB.*/

	setlocale(LC_ALL, "C");

	script_load(sc, sc->nomfich1, 1, NULL);  /* lecture recursive a priori */
	dprints("\nscript read: starting res file generation\n");
	creation(NULL, flag_save_res);

	return 0;
}
