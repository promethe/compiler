#ifndef __CC_LETO_H__
#define __CC_LETO_H__


#include <blc.h>
#include <net_message_debug_dist.h>

void kprints(const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stdout, fmt, ap);
	va_end(ap);
}


#endif
